$(document).ready(function(){
    /*$('#hipipe-arch-img').mouseout(function(e){
        var t = document.getElementById('hipipe-arch-img');
        t.src = "images/architect.jpg";
    });*/
    $('#hp-feature-1').click(function(e){
        var t = document.getElementById('hipipe-arch-img');
        t.src = "images/test-1.jpg";
        e.preventDefault();
    });
    var scrollFn = function(target, e){
            window.scroll({top:target.offset().top, left:0, behavior: 'smooth' });
                e.preventDefault();
                }
    $('#learnAboutUs').click(function(e){scrollFn.apply(this, [$('.honor-section'), e])});
    $('#menuHipipe').click(function(e){scrollFn.apply(this, [$('.hipipe-section'), e])});
    $('#menuVarpipe').click(function(e){scrollFn.apply(this, [$('.varpipe-tech'), e])});
    $('#menuTeam').click(function(e){scrollFn.apply(this, [$('.team-section'), e])});
    $('#menuTech').click(function(e){scrollFn.apply(this, [$('.tech-section'), e])});
    $('#menuHF').click(function(e){scrollFn.apply(this, [$('.hf-section'), e])});
    $('#menubelief').click(function(e){scrollFn.apply(this, [$('.belief-section'), e])});
    $('#menuProd').click(function(e){scrollFn.apply(this, [$('.prod-section'), e])});
})
